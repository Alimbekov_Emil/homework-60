import { useEffect, useState } from "react";
import Message from "../../Components/Message/Message";
import FormBlock from "../FormBlock/FormBlock";
import "./ChatBlock.css";

const ChatBlock = () => {
  const [message, setMessage] = useState([]);
  const [lastDate, setLastDate] = useState("");
  const url = "http://146.185.154.90:8000/messages";

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(url);
      if (response.ok) {
        const message = await response.json();
        setLastDate(message[message.length - 1].datetime);
        setMessage(message);
      }
    };
    fetchData().catch(console.error);
  }, []);

  useEffect(() => {
    const newMessages = () => {
      const lastUrl = url + `?datetime=${lastDate}`;
      try {
        fetch(lastUrl).then((response) => {
          return response.json();
        }).then(result => {
          if (result.length > 0) {
            setLastDate(result[result.length - 1].datetime);
            setMessage((prev) => [...prev, ...result]);
          }
        });
      } catch (e) {
        console.log(e);
      }
    };
   let interval = setInterval(() => {
      newMessages();
    }, 2000);
    return () => clearInterval(interval);
  }, [lastDate]);

  const messages = message.reverse().map((message, index) => {
    return (
      <Message
        key={index + message.author}
        author={message.author}
        message={message.message}
        date={message.datetime}
      />
    );
  });

  return (
    <div className="ChatBlock">
      <FormBlock />
      <div className="Messages">{messages}</div>
    </div>
  );
};

export default ChatBlock;
