import React, { useState } from "react";
import "./FormBlock.css";

const FormBlock = () => {
  const [inputText, setInputText] = useState("");
  const [areaText, setAreaText] = useState("");

  const HandlerChangeInput = (event) => {
    setInputText(event.target.value);
  };

  const HandlerChangeArea = (event) => {
    setAreaText(event.target.value);
  };

  const sendMessage = async (e) => {
    e.preventDefault();
    const data = new URLSearchParams();

    data.set("message", areaText);
    data.set("author", inputText);

    await fetch("http://146.185.154.90:8000/messages", {
      method: "post",
      body: data,
    });
  };

  return (
    <form className="FormBlock" onSubmit={(e) => sendMessage(e)}>
      <p>
        <input onChange={(event) => HandlerChangeInput(event)} placeholder='Введите Автора' />
      </p>
      <p>
        <textarea
          cols="100 "
          rows="5"
          onChange={(event) => HandlerChangeArea(event)}
          placeholder="Введите Ваше сообщение"
        />
      </p>
      <button type="submit">Post message</button>
    </form>
  );
};

export default FormBlock;
