import React from "react";
import "./Message.css";
import moment from "moment";

const Message = (props) => {
  return (
    <div className="Message">
      <p>Дата:{moment(props.date).format(" DDD MMMM  YYYY ,HH:mm:ss")}</p>
      <p>Автор: {props.author}</p>
      <p>Сообщение: {props.message}</p>
    </div>
  );
};

export default Message;
